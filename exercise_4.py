# Завдання 4
# Створіть клас, який описує автомобіль. Створіть клас автосалону, що містить в собі список автомобілів,
# доступних для продажу, і функцію продажу заданого автомобіля.
"""exercise 4"""


class Car:
    """Class make cars"""

    def __init__(self, make, model, price):
        self.make = make
        self.model = model
        self.price = price

    def info_car(self):
        """information about car"""
        print(f"Автомобіль марки: {self.make}, модель: {self.model}, вартість: {self.price}$")


class ShopCars:
    """Class make shop cars"""

    @staticmethod
    def get_all_cars():
        """Information about all cars"""
        for car in all_cars:
            Car.info_car(car)

    @staticmethod
    def sell_car(make, model):
        """Sell car and remove from list all_cars"""
        for car in all_cars:
            if car.model == model and car.make == make:
                all_cars.remove(car)
                print(f"Автомобіль {car.make} {car.model} був продан за {car.price}$")
                break
        else:
            print("Авто не знайденно")

    @staticmethod
    def add_car(make, model, price):
        """Add car to list all_cars"""
        new_car = Car(make, model, price)
        all_cars.append(new_car)
        print(f"Додали автомобіль марки: {make}, модель: {model}, вартість: {price}$")


# Додаємо декілька машин до бази
car_1 = Car("Nissan", "Armada", 10000)
car_2 = Car("Chevrolet", "Camaro", 8000)
car_3 = Car("KIA", "Soul", 6000)

# Список всіх стандартних машин в базі для продажу
all_cars = [car_1, car_2, car_3]

# Створюємо центр продажу машин
dealer = ShopCars()

while True:
    command = input(
        "Для перегляду всіх машин натисніть 1. "
        "Для додавання машини натисніть +. "
        "Для продажу машини та його видалення з бази натисніть -. "
        "Для виходу натисніть Enter \n"
    )
    if command == "1":
        dealer.get_all_cars()
        continue
    elif command == "+":
        dealer.add_car(
            input("Марка автомобіля: "),
            input("Модель автомобіля: "),
            int(input("Ціна автомобіля: ")),
        )
        continue
    elif command == "-":
        dealer.sell_car(input("Марка автомобіля: "),
                        input("Модель автомобіля: "))
        continue
    elif command == "":
        break
    else:
        print("Команда не знайдена. Повторіть спробу")
        continue
