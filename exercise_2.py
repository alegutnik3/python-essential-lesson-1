# Створіть клас, який описує відгук до книги. Додайте до класу книги поле – список відгуків.
# Зробіть так, щоб при виведенні книги на екран за допомогою функції print також виводилися відгуки до неї.
"""exercise 2"""


class Book:
    """Class representing a book"""

    def __init__(self, author, title, year, genre):
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre
        self.reviews = []

    def add_reviews(self):
        """Add review for book"""
        self.reviews.append(input("Напишіть комментар до книги: "))
        print("Комментар додан успішно")

    def __str__(self):
        print_reviews = "Відгуки до книги відсутні"
        if len(self.reviews) != 0:
            print_reviews = "\nВсі відгуки до книги:\n"
            for i in range(len(self.reviews)):
                print_reviews += str(i + 1) + ": " + self.reviews[i] + "\n"
        return f"Автор книги: {self.author}. Назва книги: {self.title}. Рік видання: {self.year}. Жанр: {self.genre}. {print_reviews}"

    def __repr__(self):
        return f"{self.author}, {self.title}, {self.year}, {self.genre}"

    @staticmethod
    def get_all_books():
        """Print information about all books"""
        for book in all_books:
            print(book)

    @staticmethod
    def add_book():
        """Add new book"""
        new_book = Book(
            input("Введіть прізвище автора: "),
            input("Введіть назву книги: "),
            input("Введіть рік видання: "),
            input("Введіть жанр: "),
        )
        all_books.append(new_book)
        print("Нова книга додана успішно")

    @staticmethod
    def search_book():
        """Search book in library"""
        for book in all_books:
            if book.title == search_title:
                return book
            return None


# Створюємо декілька книжок
book_1 = Book(author="George Orwell", title="1984", year=1949, genre="Dystopian")
book_2 = Book(author="Aldous Huxley", title="Brave New World", year=1932, genre="Science fiction")

# Список всіх стандартних книжок в бібліотеці
all_books = [book_1, book_2]

while True:
    commond = input(
        "Для прегляду всіх книг натисни 1. "
        "Для створення нової книги натисни +. "
        "Для додавання відгуку до книги натисни ++ \n"
        "Для виходу натисніть enter"
    )
    if commond == "1":
        Book.get_all_books()
    elif commond == "+":
        Book.add_book()
    elif commond == "++":
        search_title = input("Напишіть назву книги для пошуку: ")
        if Book.search_book() is not None:
            Book.add_reviews(Book.search_book())
        else:
            print("Книга не знайдена")
    elif commond == "":
        break
    else:
        print("Команда не знайдена")
