# Створіть клас, який описує книгу. Він повинен містити інформацію про автора, назву, рік видання та жанр.
# Створіть кілька різних книжок. Визначте для нього методи _repr_ та _str_.
"""exercise 1"""


class Book:
    """Class representing a book"""

    def __init__(self, author, title, year, genre) -> None:
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre

    def __str__(self):
        return f"Автор книги: {self.author}. Назва книги: {self.title}. Рік видання: {self.year}. Жанр: {self.genre}"

    def __repr__(self):
        return f"{self.author}, {self.title}, {self.year}, {self.genre}"


book_1 = Book(author="George Orwell", title="1984", year=1949, genre="Dystopian")
book_2 = Book(author="Aldous Huxley", title="Brave New World", year=1932, genre="Science fiction")

print(str(book_1))
print(repr(book_2))
